﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectDate.BLL.DTO;
using ProjectDate.DAL.Entities;

namespace ProjectDate.BLL.Configuration
{
    public class DbMapperConfiguration : Profile
    {
        public DbMapperConfiguration()
        {
            CreateMap<Employee, EmployeeDto>();
            CreateMap<Project, ProjectDto>();
            CreateMap<Member, MemberDto>();
            CreateMap<Position, PositionDto>();

            CreateMap<EmployeeDto, Employee>();
            CreateMap<ProjectDto, Project>();
            CreateMap<MemberDto, Member>();
            CreateMap<PositionDto, Position>();
        }
    }
}
