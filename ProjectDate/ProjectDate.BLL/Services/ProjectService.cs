﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private IProjectRepository projectRepository;
        public ProjectService(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public IEnumerable<ProjectDto> GetAll()
        {
            var projects = projectRepository.GetAll();
            if (!projects.Any())
            {
                return new List<ProjectDto>();
            }
            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDto>>(projects);
        }

        public ProjectDto Get(int id)
        {
            var project = projectRepository.Get(id);
            return Mapper.Map<ProjectDto>(project);
        }

        public ProjectDto SingleOrDefault(Func<Project, bool> predicate)
        {
            var project = projectRepository.SingleOrDefault(predicate);
            return Mapper.Map<ProjectDto>(project);
        }

        public IEnumerable<ProjectDto> Find(Expression<Func<Project, bool>> predicate)
        {
            var projects = projectRepository.Find(predicate);
            return Mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDto>>(projects);
        }

        public ProjectDto Create(ProjectDto item)
        {
            var project = Mapper.Map<Project>(item);
            projectRepository.Create(project);
            return SingleOrDefault(p => p.Name == item.Name);
        }

        public void Update(ProjectDto item)
        {
            var project = Mapper.Map<Project>(item);
            projectRepository.Update(project);
        }

        public void Delete(int id)
        {
            projectRepository.Delete(id);
        }

        public string UniquenessProject(string name)
        {
            ProjectDto position = SingleOrDefault(p => p.Name == name);
            return position != null 
                ? "Такой проект уже существует" : "True";
        }
    }

    
}
