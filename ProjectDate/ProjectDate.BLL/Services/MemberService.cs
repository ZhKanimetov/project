﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.BLL.Services
{
    public class MemberService : IMemberService
    {
        private IMemberRepository memberRepository;
        public MemberService(IMemberRepository memberRepository)
        {
            this.memberRepository = memberRepository;
        }

        public IEnumerable<MemberDto> GetAll()
        {
            var member = memberRepository.GetAll();
            return Mapper.Map<IEnumerable<Member>, IEnumerable<MemberDto>>(member);
        }

        public MemberDto Get(int id)
        {
            var member = memberRepository.Get(id);
            return Mapper.Map<MemberDto>(member);
        }

        public MemberDto SingleOrDefault(Func<Member, bool> predicate)
        {
            var member = memberRepository.SingleOrDefault(predicate);
            return Mapper.Map<MemberDto>(member);
        }

        public IEnumerable<MemberDto> Find(Expression<Func<Member, bool>> predicate)
        {
            var members = memberRepository.Find(predicate);
            return Mapper.Map<IEnumerable<Member>, IEnumerable<MemberDto>>(members);
        }

        public void Create(List<int> employeeIds, List<int> positonIds, int projectId)
        {
            if(employeeIds == null) return;
            for (int i = 0; i < employeeIds.Count(); i++)
            {
                memberRepository.Create(new Member()
                {
                    EmployeeId = employeeIds[i],
                    PositionId = positonIds[i],
                    ProjectId = projectId
                });
            }
            
        }

        public void Update(MemberDto item)
        {
            var member = Mapper.Map<Member>(item);
            memberRepository.Update(member);
        }

        public void Delete(int id)
        {
            memberRepository.Delete(id);
        }
    }
}
