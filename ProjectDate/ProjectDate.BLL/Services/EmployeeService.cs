﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Internal;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.BLL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IEmployeeRepository employeeRepository;
        private IMemberService memberService;
        public EmployeeService(IEmployeeRepository employeeRepository, IMemberService memberService)
        {
            this.employeeRepository = employeeRepository;
            this.memberService = memberService;
        }

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public IEnumerable<EmployeeDto> GetAll()
        {
            var employees = employeeRepository.GetAll();
            if(!EnumerableExtensions.Any(employees)) return new List<EmployeeDto>();
            return Mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeDto>>(employees);
        }

        public EmployeeDto Get(int id)
        {
            var employee = employeeRepository.Get(id);
            return Mapper.Map<EmployeeDto>(employee);
        }

        public EmployeeDto SingleOrDefault(Func<Employee, bool> predicate)
        {
            var employee = employeeRepository.SingleOrDefault(predicate);
            return Mapper.Map<EmployeeDto>(employee);
        }

        public IEnumerable<EmployeeDto> Find(Expression<Func<Employee, bool>> predicate)
        {
            var employees = employeeRepository.Find(predicate);
            return Mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeDto>>(employees);
        }

        public void Create(EmployeeDto item)
        {
            var employee = Mapper.Map<Employee>(item);
            employeeRepository.Create(employee);
        }

        public void Update(EmployeeDto item)
        {
            var employee = Mapper.Map<Employee>(item);
            employeeRepository.Update(employee); 
        }

        public void Delete(int id)
        {
            employeeRepository.Delete(id);
        }

        public IEnumerable<EmployeeDto> GetMember(int? projectId)
        {
            List<EmployeeDto> employee = GetAll().ToList();
            if (projectId == null) return employee;
            var members = memberService.Find(m => m.ProjectId == projectId).Select(m => m.Employee);
            employee.RemoveAll(c => members.ToList().Exists(n => n.Id == c.Id));

            return employee;
        }
    }
}
