﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Internal;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.BLL.Services
{
    public class PositionService : IPositionService
    {
        private IPositionRepository positionRepository;
        public PositionService(IPositionRepository positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public IEnumerable<PositionDto> GetAll()
        {
            var employees = positionRepository.GetAll();
            if (!employees.Any()) return new List<PositionDto>();
            return Mapper.Map<IEnumerable<Position>, IEnumerable<PositionDto>>(employees);
        }

        public PositionDto Get(int id)
        {
            var position = positionRepository.Get(id);
            return Mapper.Map<PositionDto>(position);
        }

        public PositionDto SingleOrDefault(Func<Position, bool> predicate)
        {
            var position = positionRepository.SingleOrDefault(predicate);
            return Mapper.Map<PositionDto>(position);
        }

        public IEnumerable<PositionDto> Find(Expression<Func<Position, bool>> predicate)
        {
            var position = positionRepository.Find(predicate);
            return Mapper.Map<IEnumerable<Position>, IEnumerable<PositionDto>>(position);
        }

        public void Create(PositionDto item)
        {
            var position = Mapper.Map<Position>(item);
            positionRepository.Create(position);

        }

        public void Update(PositionDto item)
        {
            var position = Mapper.Map<Position>(item);
            positionRepository.Update(position);
        }

        public void Delete(int id)
        {
            positionRepository.Delete(id);
        }

        public string UniquenessPositon(string name, int id)
        {
            PositionDto position = SingleOrDefault(p => p.Name == name);
            return position != null && id != position.Id
                ? "Такая должность уже существует" : "True";
        }
    }
}
