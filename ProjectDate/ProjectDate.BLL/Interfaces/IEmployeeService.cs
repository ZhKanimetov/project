﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ProjectDate.BLL.DTO;
using ProjectDate.DAL.Entities;

namespace ProjectDate.BLL.Interfaces
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeDto> GetAll();
        EmployeeDto Get(int id);
        EmployeeDto SingleOrDefault(Func<Employee, bool> predicate);
        IEnumerable<EmployeeDto> Find(Expression<Func<Employee, bool>> predicate);
        void Create(EmployeeDto item);
        void Update(EmployeeDto item);
        void Delete(int id);
        IEnumerable<EmployeeDto> GetMember(int? projectId);
    }
}
