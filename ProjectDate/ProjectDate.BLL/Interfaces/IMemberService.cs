﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ProjectDate.BLL.DTO;
using ProjectDate.DAL.Entities;

namespace ProjectDate.BLL.Interfaces
{
    public interface IMemberService
    {
        IEnumerable<MemberDto> GetAll();
        MemberDto Get(int id);
        MemberDto SingleOrDefault(Func<Member, bool> predicate);
        IEnumerable<MemberDto> Find(Expression<Func<Member, bool>> predicate);
        void Create(List<int> employeeIds, List<int> positonIds, int projectId);
        void Update(MemberDto item);
        void Delete(int id);
    }
}
