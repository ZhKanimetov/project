﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ProjectDate.BLL.DTO;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.BLL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDto> GetAll();
        ProjectDto Get(int id);
        ProjectDto SingleOrDefault(Func<Project, bool> predicate);
        IEnumerable<ProjectDto> Find(Expression<Func<Project, bool>> predicate);
        ProjectDto Create(ProjectDto item);
        void Update(ProjectDto item);
        void Delete(int id);
        string UniquenessProject(string name);
    }
}
