﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using ProjectDate.BLL.DTO;
using ProjectDate.DAL.Entities;

namespace ProjectDate.BLL.Interfaces
{
    public interface IPositionService
    {
        IEnumerable<PositionDto> GetAll();
        PositionDto Get(int id);
        PositionDto SingleOrDefault(Func<Position, bool> predicate);
        IEnumerable<PositionDto> Find(Expression<Func<Position, bool>> predicate);
        void Create(PositionDto item);
        void Update(PositionDto item);
        void Delete(int id);
        string UniquenessPositon(string name, int id);
    }
}
