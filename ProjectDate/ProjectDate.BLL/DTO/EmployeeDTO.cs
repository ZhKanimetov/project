﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectDate.BLL.DTO
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }
}
