﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectDate.BLL.DTO
{
    public class PositionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
