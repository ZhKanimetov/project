﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectDate.BLL.DTO
{
    public class MemberDto
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int PositionId { get; set; }

        public EmployeeDto Employee { get; set; }
        public ProjectDto Project { get; set; }
        public PositionDto Position { get; set; }
    }
}
