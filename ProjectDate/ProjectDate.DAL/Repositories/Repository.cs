﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity 
    {
        private ApplicationDbContext context { get; set; }
        protected DbSet<T> AllEntities { get; set; }

        public Repository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return AllEntities.AsNoTracking().ToList();
        }

        public virtual T Get(int id)
        {
            return AllEntities.FirstOrDefault(e => e.Id == id);
        }

        public virtual T SingleOrDefault(Func<T, bool> predicate)
        {
            return AllEntities.SingleOrDefault(predicate);
        }

        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return AllEntities.AsNoTracking().Where(predicate);
        }

        public void Create(T item)
        {
            AllEntities.Add(item);
            context.SaveChanges();
        }

        public void Update(T item)
        {
            AllEntities.Update(item);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            T entity = AllEntities.Find(id);
            if (entity != null)
                AllEntities.Remove(entity);
            context.SaveChanges();
        }
    }
}
