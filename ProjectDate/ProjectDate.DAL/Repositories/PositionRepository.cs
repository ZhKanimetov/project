﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.DAL.Repositories
{
    public class PositionRepository : Repository<Position>, IPositionRepository
    {

        public PositionRepository(ApplicationDbContext context) : base(context)
        {
            AllEntities = context.Positions;
        }
    }
}
