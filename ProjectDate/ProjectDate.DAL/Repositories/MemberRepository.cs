﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Entities;
using ProjectDate.DAL.Interfaces;

namespace ProjectDate.DAL.Repositories
{
    public class MemberRepository : Repository<Member>, IMemberRepository
    {
        public MemberRepository(ApplicationDbContext context) : base(context)
        {
            AllEntities = context.Members;
        }
        public override IEnumerable<Member> Find(Expression<Func<Member, bool>> predicate)
        {
            return AllEntities
                .Include(m => m.Employee)
                .AsNoTracking().Include(m => m.Employee).Include(m => m.Position).Where(predicate);
        }
    }
}
