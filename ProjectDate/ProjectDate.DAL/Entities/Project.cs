﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectDate.DAL.Entities
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string CustomerName { get; set; }
        public string ExecuterName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
    }
}
