﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectDate.DAL.Entities
{
    public class Employee : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
    }
}
