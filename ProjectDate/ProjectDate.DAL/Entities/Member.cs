﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ProjectDate.DAL.Entities
{
    public class Member : Entity
    {
        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }
        [ForeignKey("Position")]
        public int PositionId { get; set; }

        public Employee Employee { get; set; }
        public Project Project { get; set; }
        public Position Position { get; set; }
    }
}
