﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectDate.DAL.Entities;

namespace ProjectDate.DAL.Interfaces
{
    public interface IProjectRepository : IRepository<Project>
    {
    }
}
