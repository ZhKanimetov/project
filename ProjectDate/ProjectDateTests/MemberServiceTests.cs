﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Services;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Interfaces;
using ProjectDate.DAL.Repositories;
using ProjectDateTests.Configuration;
using Xunit;

namespace ProjectDateTests
{
    public class MemberServiceTests
    {
        public MemberServiceTests()
        {
            AutoMapperConfiguration.Configure();
        }

        [Fact]
        public void AddMemder()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AddMemder")
                .Options;
            MemberDto member = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IMemberRepository memberRepository = new MemberRepository(context);
                var service = new MemberService(memberRepository);
                MemberDto createdMember = service.SingleOrDefault
                    (p => p.EmployeeId == member.Employee.Id && p.ProjectId == member.Project.Id);
                Assert.NotNull(createdMember);
                Assert.Equal(member.Position.Id, createdMember.PositionId);
            }

        }

        [Fact]
        public void DeleteMember()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "DeleteMember")
                .Options;
            MemberDto member = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IMemberRepository memberRepository = new MemberRepository(context);
                var service = new MemberService(memberRepository);
                MemberDto createdMember = service.SingleOrDefault
                    (p => p.EmployeeId == member.Employee.Id && p.ProjectId == member.Project.Id);
                service.Delete(createdMember.Id);
                createdMember = service.Get(createdMember.Id);
                Assert.Null(createdMember);
            }
        }


        public static MemberDto Add(DbContextOptions<ApplicationDbContext> options)
        {
            EmployeeDto employee = EmployeeServiceTests.Add(options);
            PositionDto position = PositionServiceTests.Add(options);
            ProjectDto project = ProjectServiceTests.Add(options);
            List<int> employeeId = new List<int>(){{employee.Id}};
            List<int> positionId = new List<int>(){{ position.Id}};
            using (var context = new ApplicationDbContext(options))
            {
                IMemberRepository memberRepository = new MemberRepository(context);
                var service = new MemberService(memberRepository);
                service.Create(employeeId, positionId, project.Id );
            }

            return new MemberDto()
            {
                Employee = employee,
                Project = project,
                Position = position
            };
        }
    }
}
