﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Services;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Interfaces;
using ProjectDate.DAL.Repositories;
using Xunit;

namespace ProjectDateTests.Configuration
{
    public class PositionServiceTests
    {
        public PositionServiceTests()
        {
            AutoMapperConfiguration.Configure();
        }

        [Fact]
        public void CreatePosition()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreatePosition")
                .Options;
            var project = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                PositionDto createdPosition = service.SingleOrDefault(p => p.Name == project.Name);
                Assert.NotNull(createdPosition);
                Assert.Equal(project.Name, createdPosition.Name);
            }
        }

        [Fact]
        public void DeletePosition()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreatePosition")
                .Options;
            var project = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                PositionDto createdPosition = service.SingleOrDefault(p => p.Name == project.Name);
                service.Delete(createdPosition.Id);
                createdPosition = service.Get(createdPosition.Id);
                Assert.Null(createdPosition);
            }
        }

        [Fact]
        public void GetAllPositions()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GetAllPositions")
                .Options;
            List<PositionDto> positions = new List<PositionDto>();
            positions.Add(new PositionDto(){Name = "Капитан"});
            positions.Add(new PositionDto(){Name = "Аналитик"});
            positions.Add(new PositionDto(){Name = "Программист"});
            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                foreach (var position in positions)
                {
                    service.Create(position);
                }
            }

            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                var result = service.GetAll();
                Assert.Equal(result.Count(),positions.Count);
            }
        }

        [Fact]
        public void UniquenessPosition()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UniquenessPosition")
                .Options;
            var position = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                string result = service.UniquenessPositon(position.Name,0);
                string result2 = service.UniquenessPositon("NotCreated",0);
                Assert.Equal("Такая должность уже существует", result);
                Assert.Equal("True", result2);
            }
        }

        public static PositionDto Add(DbContextOptions<ApplicationDbContext> options)
        {
            PositionDto position = new PositionDto()
            {
                Name = "Капитан"
            };
            using (var context = new ApplicationDbContext(options))
            {
                IPositionRepository positionRepository = new PositionRepository(context);
                var service = new PositionService(positionRepository);
                service.Create(position);
            }

            return position;
        }
    }
}
