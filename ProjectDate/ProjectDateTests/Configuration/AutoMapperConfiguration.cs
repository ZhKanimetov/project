﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ProjectDate.BLL.Configuration;

namespace ProjectDateTests.Configuration
{
    public class AutoMapperConfiguration
    {
        public static bool isConfigure;
        public static void Configure()
        {
            if (!isConfigure)
            {
                try
                {
                    Mapper.Initialize(x =>
                    {
                        x.AddProfile<DbMapperConfiguration>();
                    });

                    Mapper.Configuration.AssertConfigurationIsValid();
                    isConfigure = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }
            }
        }
    }
}
