﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Services;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Interfaces;
using ProjectDate.DAL.Repositories;
using ProjectDateTests.Configuration;
using Xunit;

namespace ProjectDateTests
{
    public class EmployeeServiceTests
    {
        public EmployeeServiceTests()
        {
            AutoMapperConfiguration.Configure();
        }

        [Fact]
        public void CreateEmployee()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreateEmployee")
                .Options;
            var employee = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IEmployeeRepository employeeRepository = new EmployeeRepository(context);
                var service = new EmployeeService(employeeRepository);
                EmployeeDto createdProject = service.SingleOrDefault(p => p.Name == employee.Name);
                Assert.NotNull(createdProject);
                Assert.Equal(employee.Email, createdProject.Email);
            }
        }

        [Fact]
        public void DeleteEmployee()
        {

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "DeleteEmployee")
                .Options;
            var employee = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IEmployeeRepository employeeRepository = new EmployeeRepository(context);
                var service = new EmployeeService(employeeRepository);
                EmployeeDto createdProject = service.SingleOrDefault(p => p.Name == employee.Name);
                service.Delete(createdProject.Id);
                createdProject = service.Get(createdProject.Id);
                Assert.Null(createdProject);
            }
        }

        [Fact]
        public void GetMembers()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GetMembers")
                .Options;
            var member = MemberServiceTests.Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IEmployeeRepository employeeRepository = new EmployeeRepository(context);
                IMemberRepository memberRepository = new MemberRepository(context);
                var memberService = new MemberService(memberRepository);
                var service = new EmployeeService(employeeRepository, memberService);
                var result = service.GetMember(member.ProjectId);
                Assert.Equal(1, result.Count());
                Assert.Equal("Zhakhar", result.First().Name);
            }
        }

        public static EmployeeDto Add(DbContextOptions<ApplicationDbContext> options)
        {
            EmployeeDto employee = new EmployeeDto()
            {
                Name = "Zhakhar",
                Email = "zhakhar.kanimetov@gmail.com",
                Lastname = "Nurlanovich",
                Surname = "Kanimetov"
            };
            using (var context = new ApplicationDbContext(options))
            {
                IEmployeeRepository employeeRepository = new EmployeeRepository(context);
                var service = new EmployeeService(employeeRepository);
                service.Create(employee);
            }

            return employee;
        }
    }
}
