﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using ProjectDate.BLL.Configuration;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Services;
using ProjectDate.DAL.EF;
using ProjectDate.DAL.Interfaces;
using ProjectDate.DAL.Repositories;
using ProjectDateTests.Configuration;
using Xunit;
using Xunit.Sdk;

namespace ProjectDateTests
{
    public class ProjectServiceTests
    {
        public ProjectServiceTests()
        {
            AutoMapperConfiguration.Configure();
        }

        [Fact]
        public void CreateProject()
        {
            
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CreateProject")
                .Options;
            var project = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                ProjectDto createdProject = service.SingleOrDefault(p => p.Name == project.Name);
                Assert.NotNull(createdProject);
                Assert.Equal(project.Rating, createdProject.Rating);
            }
        }

        [Fact]
        public void DeleteProject()
        {
            
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "DeleteProject")
                .Options;
            var project = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                ProjectDto createdProject = service.SingleOrDefault(p => p.Name == project.Name);
                service.Delete(createdProject.Id);
                createdProject = service.Get(createdProject.Id);
                Assert.Null(createdProject);
            }
        }

        [Fact]
        public void UniquenessProject()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "UniquenessProject")
                .Options;
            var project = Add(options);
            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                string result = service.UniquenessProject(project.Name);
                string result2 = service.UniquenessProject("NotCreated");
                Assert.Equal("Такой проект уже существует",result );
                Assert.Equal("True", result2);
            }
        }

        [Fact]
        public void GetAllProject()
        {
            List<ProjectDto> projects = new List<ProjectDto>();
            projects.Add(new ProjectDto()
            {
                CustomerName = "Customer1",
                Comment = "Comment1",
                ExecuterName = "Company1",
                EndDate = Convert.ToDateTime("13.11.2018"),
                Name = "Project1",
                Rating = 5,
                StartDate = Convert.ToDateTime("13.11.2018")
            });
            projects.Add(new ProjectDto()
            {
                CustomerName = "Customer2",
                Comment = "Comment2",
                ExecuterName = "Company2",
                EndDate = Convert.ToDateTime("13.11.2018"),
                Name = "Project2",
                Rating = 4,
                StartDate = Convert.ToDateTime("13.11.2018")
            });
            projects.Add(new ProjectDto()
            {
                CustomerName = "Customer3",
                Comment = "Comment3",
                ExecuterName = "Company3",
                EndDate = Convert.ToDateTime("13.11.2018"),
                Name = "Project3",
                Rating = 3,
                StartDate = Convert.ToDateTime("13.11.2018")
            });
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "GetAllProject")
                .Options;
            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                foreach (var project in projects)
                {
                    service.Create(project);
                }
            }

            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                var result = service.GetAll();
                Assert.Equal(projects.Count, result.Count());
            }
        }

        public static ProjectDto Add(DbContextOptions<ApplicationDbContext> options)
        {
            ProjectDto project = new ProjectDto()
            {
                CustomerName = "Saibers",
                Comment = "From Zhakhar",
                ExecuterName = "ZhakharCompany",
                EndDate = Convert.ToDateTime("13.11.2018"),
                Name = "ProjectDate",
                Rating = 5,
                StartDate = Convert.ToDateTime("13.11.2018")
            };
            using (var context = new ApplicationDbContext(options))
            {
                IProjectRepository projectRepository = new ProjectRepository(context);
                var service = new ProjectService(projectRepository);
                service.Create(project);
            }

            return project;
        }
    }
}
