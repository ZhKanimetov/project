﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDate.WEB.Models.MemberViewModels
{
    public class MemberCreateModel
    {
        public int EmployeeId { get; set; }
        public int PositionId { get; set; }
        public int ProjectId { get; set; }

        public string Name { get; set; }
    }
}
