﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectDate.WEB.Models.EmployeeViewModels;
using ProjectDate.WEB.Models.PositionViewModels;
using ProjectDate.WEB.Models.ProjectViewModels;

namespace ProjectDate.WEB.Models.MemberViewModels
{
    public class MemberViewModel
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int EmployeeId { get; set; }
        public int PositionId { get; set; }

        public EmployeeViewModel Employee { get; set; }
        public ProjectViewModel Project { get; set; }
        public PositionViewModel Position { get; set; }
    }
}
