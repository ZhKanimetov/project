﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDate.WEB.Models.ProjectViewModels
{
    public class ProjectCreateModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите название проетк")]
        [Remote("UniquenessProject", "Validation")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите имя заказчика")]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Введите имя исполнителя")]
        public string ExecuterName { get; set; }
        [Required(ErrorMessage = "Введите дату начала")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Введите дату окончания")]
        public DateTime EndDate { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }

        public List<int> EmployeeId { get; set; }
        [Required]
        public List<int> PositionId { get; set; }
    }
}
