﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDate.WEB.Models.EmployeeViewModels
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите имя сотрудника")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите фамилию сотрудника")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Введите отчество сотрудника")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Введите почту сотрудника")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$", ErrorMessage = "Введите коректную почту")]
        public string Email { get; set; }
    }
}
