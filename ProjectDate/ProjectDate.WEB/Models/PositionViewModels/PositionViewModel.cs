﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProjectDate.WEB.Models.PositionViewModels
{
    public class PositionViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Введите должность")]
        [Remote("UniquenessPositon", "Validation", AdditionalFields = "Id")]
        public string Name { get; set; }
    }
}
