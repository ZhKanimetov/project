﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.WEB.Models;
using ProjectDate.WEB.Models.MemberViewModels;
using ProjectDate.WEB.Models.PositionViewModels;
using ProjectDate.WEB.Models.ProjectViewModels;

namespace ProjectDate.WEB.Controllers
{
    public class HomeController : Controller
    {
        private IProjectService projectService;
        private IPositionService positionService;
        private IMemberService memberService;
        private IEmployeeService employeeService;

        public HomeController(
            IProjectService projectService,
            IPositionService positionService,
            IMemberService memberService,
            IEmployeeService employeeService)
        {
            this.projectService = projectService;
            this.positionService = positionService;
            this.memberService = memberService;
            this.employeeService = employeeService;
        }

        public IActionResult Index()
        {
            IEnumerable<ProjectDto> projectsDto = projectService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProjectDto, ProjectViewModel>()).CreateMapper();
            var projects = mapper.Map<IEnumerable<ProjectDto>, List<ProjectViewModel>>(projectsDto);
            return View(projects);
        }

        public IActionResult Create()
        {
            IEnumerable<PositionDto> positionDtos = positionService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PositionDto, PositionViewModel>()).CreateMapper();
            ViewBag.Positions = mapper.Map<IEnumerable<PositionDto>, List<PositionViewModel>>(positionDtos);
            return View();
        }

        [HttpPost]
        public IActionResult Create(ProjectCreateModel model)
        {
            var createdProject = projectService.Create(new ProjectDto()
            {
                Comment = model.Comment,
                CustomerName = model.CustomerName,
                EndDate = model.EndDate,
                ExecuterName = model.ExecuterName,
                Name = model.Name,
                Rating = model.Rating,
                StartDate = model.StartDate
            });
            memberService.Create(model.EmployeeId,model.PositionId, createdProject.Id);
            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult GetMembers(int projectId)
        {
            var members = memberService.Find(m => m.ProjectId == projectId).ToList();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<MemberDto, MemberViewModel>()).CreateMapper();
            List<MemberViewModel> model = mapper.Map<List<MemberDto>, List<MemberViewModel>>(members);
            ViewBag.ProjectId = projectId;
            return PartialView(model);
        }

        public IActionResult AddMember(int employeeId, int projectId)
        {
            IEnumerable<PositionDto> positionDtos = positionService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PositionDto, PositionViewModel>()).CreateMapper();
            ViewBag.Positions = mapper.Map<IEnumerable<PositionDto>, List<PositionViewModel>>(positionDtos);
            return PartialView(new MemberCreateModel()
            {
                ProjectId = projectId,
                EmployeeId = employeeService.Get(employeeId).Id,
                Name = employeeService.Get(employeeId).Name
            });
        }

        [HttpPost]
        public void AddMember(int employeeId, int positionId, int projectId)
        {
            memberService.Create(new List<int>(){{ employeeId } }, new List<int>(){{ positionId } }, projectId);
        }

        [HttpPost]
        public string Delete(int id)
        {
            projectService.Delete(id);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        [HttpPost]
        public string DeleteMember(int id)
        {
            memberService.Delete(id);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public IActionResult Update(int projectId)
        {
            var project = projectService.Get(projectId);
            ProjectCreateModel model = Mapper.Map<ProjectCreateModel>(project);
            return View(model);
        }

        [HttpPost]
        public IActionResult Update(ProjectCreateModel model)
        {
            var project = Mapper.Map<ProjectDto>(model);
            projectService.Update(project);
            return RedirectToAction("Index");
        }
    }
}
