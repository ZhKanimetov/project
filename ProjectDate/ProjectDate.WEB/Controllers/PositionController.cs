﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.WEB.Models.EmployeeViewModels;
using ProjectDate.WEB.Models.PositionViewModels;

namespace ProjectDate.WEB.Controllers
{
    public class PositionController : Controller
    {
        private IPositionService positionService;

        public PositionController(IPositionService positionService)
        {
            this.positionService = positionService;
        }

        public IActionResult Index()
        {
            IEnumerable<PositionDto> positionDtos = positionService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<PositionDto, PositionViewModel>()).CreateMapper();
            var positions = mapper.Map<IEnumerable<PositionDto>, List<PositionViewModel>>(positionDtos);
            return View(positions);
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(PositionViewModel model)
        {
            if (ModelState.IsValid)
            {
                PositionDto position = Mapper.Map<PositionDto>(model);
                positionService.Create(position);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            positionService.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Update(int positionId)
        {
            var position = positionService.Get(positionId);
            PositionViewModel model = Mapper.Map<PositionViewModel>(position);
            return PartialView(model);
        }

        [HttpPost]
        public IActionResult Update(PositionViewModel model)
        {
            var position = Mapper.Map<PositionDto>(model);
            positionService.Update(position);
            return RedirectToAction("Index");
        }
    }
}