﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectDate.BLL.Interfaces;

namespace ProjectDate.WEB.Controllers
{
    public class ValidationController : Controller
    {
        private IPositionService positionService;
        private IProjectService projectService;

        public ValidationController(IPositionService positionService, IProjectService projectService)
        {
            this.positionService = positionService;
            this.projectService = projectService;
        }

        public IActionResult UniquenessPositon(string name, int id)
        {
            string message = positionService.UniquenessPositon(name, id);
            if (Boolean.TryParse(message, out var result))
            {
                return Json(result);
            }

            return Json(message);
        }

        public IActionResult UniquenessProject(string name)
        {
            string message = projectService.UniquenessProject(name);
            if (Boolean.TryParse(message, out var result))
            {
                return Json(result);
            }

            return Json(message);
        }
    }
}