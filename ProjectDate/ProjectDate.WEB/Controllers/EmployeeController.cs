﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectDate.BLL.DTO;
using ProjectDate.BLL.Interfaces;
using ProjectDate.WEB.Models.EmployeeViewModels;
using ProjectDate.WEB.Models.PositionViewModels;
using ProjectDate.WEB.Models.ProjectViewModels;

namespace ProjectDate.WEB.Controllers
{
    public class EmployeeController : Controller
    {
        private IEmployeeService employeeService;
        private IPositionService positionService;
        

        public EmployeeController(IEmployeeService employeeService, IPositionService positionService)
        {
            this.employeeService = employeeService;
            this.positionService = positionService;
        }

        public IActionResult Index()
        {
            IEnumerable<EmployeeDto> employeesDto = employeeService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<EmployeeDto, EmployeeViewModel>()).CreateMapper();
            var employees = mapper.Map<IEnumerable<EmployeeDto>, List<EmployeeViewModel>>(employeesDto);
            return View(employees);
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                EmployeeDto employee = Mapper.Map<EmployeeDto>(model);
                employeeService.Create(employee);
                return RedirectToAction("Index");
            }

            return View();
        }

        public string SelectMember(int? projectId)
        {
            IEnumerable<EmployeeDto> employeesDto = employeeService.GetMember(projectId);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<EmployeeDto, EmployeeViewModel>()).CreateMapper();
            var employees = mapper.Map<IEnumerable<EmployeeDto>, List<EmployeeViewModel>>(employeesDto);
            string json = JsonConvert.SerializeObject(employees);
            return JsonConvert.SerializeObject(json,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public IActionResult Delete(int id)
        {
            employeeService.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Update(int employeeId)
        {
            var employee = employeeService.Get(employeeId);
            EmployeeViewModel model = Mapper.Map<EmployeeViewModel>(employee);
            return PartialView(model);
        }

        [HttpPost]
        public IActionResult Update(EmployeeViewModel model)
        {
            var employee = Mapper.Map<EmployeeDto>(model);
            employeeService.Update(employee);
            return RedirectToAction("Index");
        }
    }
}