﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectDate.BLL.DTO;
using ProjectDate.WEB.Models.EmployeeViewModels;
using ProjectDate.WEB.Models.MemberViewModels;
using ProjectDate.WEB.Models.PositionViewModels;
using ProjectDate.WEB.Models.ProjectViewModels;

namespace ProjectDate.WEB.Configuration
{
    public class WebAutomapperConfiguration : Profile
    {
        public WebAutomapperConfiguration()
        {
            CreateMap<EmployeeViewModel, EmployeeDto>();
            CreateMap<ProjectViewModel, ProjectDto>();
            CreateMap<ProjectCreateModel, ProjectDto>();
            CreateMap<MemberViewModel, MemberDto>()
                .ForMember(x => x.Employee, s => s.Ignore())
                .ForMember(x => x.Position, s => s.Ignore());
            CreateMap<PositionViewModel, PositionDto>();

            CreateMap<EmployeeDto, EmployeeViewModel>();
            CreateMap<ProjectDto, ProjectViewModel>();
            CreateMap<ProjectDto, ProjectCreateModel>();
            CreateMap<MemberDto, MemberViewModel>()
                .ForMember(x => x.Employee, opt => opt.MapFrom(s => s.Employee))
                .ForMember(x => x.Position, opt => opt.MapFrom(s => s.Position));
            CreateMap<PositionDto, PositionViewModel>();
        }
    }
}
