﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectDate.BLL.Configuration;

namespace ProjectDate.WEB.Configuration
{
    public static class AutomapperInit
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<WebAutomapperConfiguration>();
                cfg.AddProfile<DbMapperConfiguration>();
            });
            //Mapper.AssertConfigurationIsValid();
        }
    }
}
